<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150521001630 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE orders (
              id int(11) NOT NULL,
              choose_ticket_type_id int(11) DEFAULT NULL,
              merchant_id int(11) DEFAULT NULL,
              amount double NOT NULL,
              account varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              aggregator_merchant_id varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              choose_tickets longtext COLLATE utf8_unicode_ci NOT NULL,
              payment_method varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              state INTEGER NOT NULL DEFAULT 0,
              created_at datetime NOT NULL,
              updated_at datetime NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ');

        $this->addSql('
            ALTER TABLE orders
              ADD PRIMARY KEY (id),
              ADD KEY IDX_E283F8D8645727EB (choose_ticket_type_id),
              ADD KEY IDX_E283F8D86796D554 (merchant_id),
              ADD INDEX IDX_7461B040025DC47271 (state);
        ');

        $this->addSql('ALTER TABLE orders MODIFY id int(11) NOT NULL AUTO_INCREMENT;');

        $this->addSql('
            ALTER TABLE orders
              ADD CONSTRAINT FK_E283F8D86796D554 FOREIGN KEY (merchant_id) REFERENCES merchants (id),
              ADD CONSTRAINT FK_E283F8D8645727EB FOREIGN KEY (choose_ticket_type_id) REFERENCES ticket_types (id);
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E283F8D86796D554');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E283F8D8645727EB');
        $this->addSql('DROP TABLE orders');
    }
}
