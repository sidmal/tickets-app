<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151002235545 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE merchants ADD COLUMN address TEXT NULL DEFAULT NULL');
        $this->addSql('ALTER TABLE merchants ADD COLUMN how_to_get TEXT NULL DEFAULT NULL');

        $this->addSql('
            UPDATE merchants SET
                address = \'Проспект Просвещения<br />Санкт-Петербург, пос. Парголово, ул. Ломоносова д.5\',
                how_to_get = \'От станции метро Проспект Просвещения двигаться по Выборгскому шоссе в сторону области\'
            WHERE name = \'Музей восстания машин\';

        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE merchants DROP COLUMN address');
        $this->addSql('ALTER TABLE merchants DROP COLUMN how_to_get');
    }
}
