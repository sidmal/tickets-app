<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150819155556 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE merchants ADD COLUMN related_user_id INTEGER(11) DEFAULT NULL');
        $this->addSql('ALTER TABLE merchants ADD CONSTRAINT FK_18F5340803B305BE FOREIGN KEY (related_user_id) REFERENCES fos_user_user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE merchants DROP FOREIGN KEY FK_18F5340803B305BE');
        $this->addSql('ALTER TABLE merchants DROP COLUMN related_user_id');
    }
}
