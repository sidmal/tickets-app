<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150522145402 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE merchants ADD COLUMN age_limit VARCHAR(10) NOT NULL');
        $this->addSql('UPDATE merchants SET age_limit = \'16+\' WHERE name = \'Музей восстания машин\'');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE merchants DROP COLUMN age_limit');
        $this->addSql('UPDATE merchants SET age_limit = \'\' WHERE name = \'Музей восстания машин\'');
    }
}
