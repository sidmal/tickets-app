<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150520133808 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $key;

    protected function getKey()
    {
        if (!$this->key) {
            $this->key = md5(uniqid(rand(), true));
        }

        return $this->key;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $date = (new \DateTime('now'))->format('Y-m-d H:i:s');

        $this->addSql('
            INSERT INTO merchants(
                name,
                promo,
                access_token,
                created_at,
                updated_at
            )
            VALUES(
                \'Музей восстания машин\',
                \'Музей восстания машин - это уникальное арт-пространство, посвященное концепту робототехники будущего, самобытный музей роботов, в котором каждый найдет близкий себе символ, связанный с воспоминаниями о прошлом и о... будущем.\',
                \''. $this->getKey() .'\',
                \''. $date .'\',
                \''. $date .'\'
            );
        ');

        $this->addSql('
            INSERT INTO ticket_types(name, description, merchant_id, created_at, updated_at)
            VALUES
                (\'Входной билет\', \'Билет на 1 посещение на 1 персону\', (SELECT id FROM merchants WHERE access_token = \''. $this->getKey() .'\'), \''. $date .'\', \''. $date .'\'),
                (\'Экскурсионный билет<br />на группу 1 - 20 человек\', \'Билет на 1 посещение на 1 персону\', (SELECT id FROM merchants WHERE access_token = \''. $this->getKey() .'\'), \''. $date .'\', \''. $date .'\');
        ');

        $this->addSql('
            INSERT INTO periods(name, merchant_id, created_at, updated_at)
            VALUES
                (\'90 дней\', (SELECT id FROM merchants WHERE access_token = \''. $this->getKey() .'\'), \''. $date .'\', \''. $date .'\');
        ');

        $this->addSql('
            INSERT INTO tickets(name, price, type_id, merchant_id, created_at, updated_at)
            VALUES
                (\'Взрослый\', 450, (SELECT id FROM ticket_types WHERE name = \'Входной билет\'), (SELECT id FROM merchants WHERE access_token = \''. $this->getKey() .'\'), \''. $date .'\', \''. $date .'\'),
                (\'Детский\', 225, (SELECT id FROM ticket_types WHERE name = \'Входной билет\'), (SELECT id FROM merchants WHERE access_token = \''. $this->getKey() .'\'), \''. $date .'\', \''. $date .'\'),
                (\'Льготный\', 270, (SELECT id FROM ticket_types WHERE name = \'Входной билет\'), (SELECT id FROM merchants WHERE access_token = \''. $this->getKey() .'\'), \''. $date .'\', \''. $date .'\'),
                (\'Взрослый\', 900, (SELECT id FROM ticket_types WHERE name LIKE \'Экскурсионный билет%\'), (SELECT id FROM merchants WHERE access_token = \''. $this->getKey() .'\'), \''. $date .'\', \''. $date .'\');
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM tickets WHERE name IN (\'Взрослый\', \'Детский\', \'Льготный\')');
        $this->addSql('DELETE FROM ticket_types WHERE name IN (\'Входной билет\', \'Экскурсионный билет<br />на группу 1 - 20 человек\')');
        $this->addSql('DELETE FROM periods WHERE name =\'30 дней\'');
        $this->addSql('DELETE FROM merchants WHERE access_token = \''.$this->getKey().'\'');
    }
}
