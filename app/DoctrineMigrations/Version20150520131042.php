<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150520131042 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
            CREATE TABLE merchants (
              id int(11) NOT NULL AUTO_INCREMENT,
              name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              promo longtext COLLATE utf8_unicode_ci NOT NULL,
              periods_description longtext COLLATE utf8_unicode_ci NULL,
              access_token varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              payment_aggregator_merchant_id varchar(255) COLLATE utf8_unicode_ci NULL,
              payment_aggregator_merchant_secret_key varchar(255) COLLATE utf8_unicode_ci NULL,
              created_at datetime NOT NULL,
              updated_at datetime NOT NULL,

              PRIMARY KEY (id),
              UNIQUE KEY UNIQ_CC77B6C0B6A2DD68 (access_token)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ');

        $this->addSql('
            CREATE TABLE ticket_types (
              id int(11) NOT NULL AUTO_INCREMENT,
              name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              description longtext COLLATE utf8_unicode_ci NOT NULL,
              created_at datetime NOT NULL,
              updated_at datetime NOT NULL,
              merchant_id int(11) DEFAULT NULL,

              PRIMARY KEY (id),
              KEY IDX_7100EABB6796D554 (merchant_id),
              CONSTRAINT FK_7100EABB6796D554 FOREIGN KEY (merchant_id) REFERENCES merchants (id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ');

        $this->addSql('
            CREATE TABLE tickets (
              id int(11) NOT NULL AUTO_INCREMENT,
              name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              price double NOT NULL,
              created_at datetime NOT NULL,
              updated_at datetime NOT NULL,
              type_id int(11) DEFAULT NULL,
              merchant_id int(11) DEFAULT NULL,

              PRIMARY KEY (id),
              KEY IDX_54469DF4C54C8C93 (type_id),
              KEY IDX_54469DF46796D554 (merchant_id),
              CONSTRAINT FK_54469DF46796D554 FOREIGN KEY (merchant_id) REFERENCES merchants (id),
              CONSTRAINT FK_54469DF4C54C8C93 FOREIGN KEY (type_id) REFERENCES ticket_types (id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ');

        $this->addSql('
            CREATE TABLE periods (
              id int(11) NOT NULL AUTO_INCREMENT,
              name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
              created_at datetime NOT NULL,
              updated_at datetime NOT NULL,
              merchant_id int(11) DEFAULT NULL,

              PRIMARY KEY (id),
              KEY IDX_671798A26796D554 (merchant_id),
              CONSTRAINT FK_671798A26796D554 FOREIGN KEY (merchant_id) REFERENCES merchants (id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE periods DROP FOREIGN KEY FK_671798A26796D554');
        $this->addSql('ALTER TABLE tickets DROP FOREIGN KEY FK_54469DF46796D554');
        $this->addSql('ALTER TABLE tickets DROP FOREIGN KEY FK_54469DF4C54C8C93');
        $this->addSql('ALTER TABLE ticket_types DROP FOREIGN KEY FK_7100EABB6796D554');
        $this->addSql('DROP TABLE periods');
        $this->addSql('DROP TABLE tickets');
        $this->addSql('DROP TABLE ticket_types');
        $this->addSql('DROP TABLE merchants');
    }
}
