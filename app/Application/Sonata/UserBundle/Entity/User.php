<?php

/**
 * This file is part of the <name> project.
 *
 * (c) <yourname> <youremail>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Tickets\TicketChooserBundle\Entity\Merchants;

/**
 * Class User
 * @package Application\Sonata\UserBundle\Entity
 *
 * @ORM\Table(name="tickets_user")
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="Tickets\TicketChooserBundle\Entity\Merchants", mappedBy="relatedUser")
     */
    protected $merchants;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->merchants = new ArrayCollection();

        parent::__construct();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add merchants
     *
     * @param Merchants $merchants
     * @return User
     */
    public function addMerchant(Merchants $merchants)
    {
        $this->merchants[] = $merchants;

        return $this;
    }

    /**
     * Remove merchants
     *
     * @param Merchants $merchants
     */
    public function removeMerchant(Merchants $merchants)
    {
        $this->merchants->removeElement($merchants);
    }

    /**
     * Get merchants
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMerchants()
    {
        return $this->merchants;
    }
}
