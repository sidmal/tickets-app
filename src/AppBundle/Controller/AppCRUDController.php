<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 30.05.15
 * Time: 0:21
 */

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Tickets\TicketChooserBundle\Entity\Orders;
use Tickets\TicketChooserBundle\Entity\Tickets;
use Tickets\TicketChooserBundle\Entity\TicketTypes;

class AppCRUDController extends CRUDController
{
    /**
     * {@inheritdoc}
     */
    public function listAction(Request $request = null)
    {
        return parent::listAction($request);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteAction($id, Request $request = null)
    {
        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if ($object instanceof Orders) {
            return parent::deleteAction($id, $request);
        }

        if (false === $this->admin->isGranted('EDIT', $object->getMerchant())) {
            throw new AccessDeniedException();
        }

        $preResponse = $this->preDelete($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        if ($this->getRestMethod($request) === 'DELETE') {
            $this->validateCsrfToken('sonata.delete', $request);

            $objectName = $this->admin->toString($object);

            try {
                $this->admin->delete($object);

                $this->addFlash(
                    'sonata_flash_success',
                    $this->admin->trans(
                        'flash_delete_success',
                        ['%name%' => $this->escapeHtml($objectName)],
                        'SonataAdminBundle'
                    )
                );
            } catch (ModelManagerException $e) {
                $this->handleModelManagerException($e);

                $this->addFlash(
                    'sonata_flash_error',
                    $this->admin->trans(
                        'flash_delete_error',
                        ['%name%' => $this->escapeHtml($objectName)],
                        'SonataAdminBundle'
                    )
                );
            }

            return $this->redirectTo($object, $request);
        }

        return $this->render($this->admin->getTemplate('delete'), array(
            'object'     => $object,
            'action'     => 'delete',
            'csrf_token' => $this->getCsrfToken('sonata.delete'),
        ), null, $request);
    }

    /**
     * {@inheritdoc}
     */
    public function editAction($id = null, Request $request = null)
    {
        $templateKey = 'edit';

        $id = $request->get($this->admin->getIdParameter());

        /** @var Tickets|TicketTypes|Orders $object */
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object->getMerchant())) {
            throw new AccessDeniedException();
        }

        $preResponse = $this->preEdit($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $isFormValid = $form->isValid();

            if ($isFormValid) {
                try {
                    $object = $this->admin->update($object);

                    $this->addFlash(
                        'sonata_flash_success',
                        $this->admin->trans(
                            'flash_edit_success',
                            ['%name%' => $this->escapeHtml($this->admin->toString($object))],
                            'SonataAdminBundle'
                        )
                    );

                    return $this->redirectTo($object, $request);
                } catch(ModelManagerException $e) {
                    $this->handleModelManagerException($e); $isFormValid = false;
                }
            }

            if (!$isFormValid) {
                $this->addFlash(
                    'sonata_flash_error',
                    $this->admin->trans(
                        'flash_edit_error',
                        ['%name%' => $this->escapeHtml($this->admin->toString($object))],
                        'SonataAdminBundle'
                    )
                );
            }
        }

        $view = $form->createView();

        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render(
            $this->admin->getTemplate($templateKey),
            [
                'action' => 'edit',
                'form'   => $view,
                'object' => $object
            ], null, $request);
    }

    /**
     * {@inheritdoc}
     */
    public function createAction(Request $request = null)
    {
        return parent::createAction($request);
    }

    /**
     * {@inheritdoc}
     */
    public function showAction($id = null, Request $request = null)
    {
        return parent::showAction($id, $request);
    }
}