<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 19.08.15
 * Time: 16:46
 */

namespace AppBundle\Admin;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\Admin;

class AppSonataAdmin extends Admin
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     * @return $this
     */
    public function setEm(EntityManager $em)
    {
        $this->em = $em;

        return $this;
    }

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        /** @var QueryBuilder $query */
        $query = parent::createQuery($context);

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            return $query;
        }

        $merchants = $this->getAllowedMerchants();

        if (empty($merchants) || count($merchants) <= 0) {
            return $query;
        }

        $query->setParameters(new ArrayCollection())->where($query->expr()->in($query->getRootAliases()[0].'.merchant', $merchants));

        return $query;
    }

    protected function getAllowedMerchants()
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
        $merchantsRepository = $em->getRepository('TicketsTicketChooserBundle:Merchants');

        $merchants = [];

        foreach ($merchantsRepository->findAll() as $merchant) {
            if ($this->isGranted('EDIT', $merchant)) {
                $merchants[] = $merchant->getId();
            }
        }

        return $merchants;
    }
}