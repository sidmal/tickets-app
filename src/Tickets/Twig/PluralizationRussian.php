<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 01.08.15
 * Time: 14:43
 */

namespace Tickets\Twig;

class PluralizationRussian extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('pluralization_russian', array($this, 'pluralizationRussian')),
        ];
    }

    public function pluralizationRussian($count, $endings = '')
    {
        list($endingOne, $endingTwo, $endingThree) = explode(',', $endings);

        $count = abs($count);
        $count = $count % 100;

        if (($count > 4) && ($count < 21)) {
            return $endingThree;
        }

        $count = $count % 10;
        if (($count == 0) || ($count > 4)) {
            return $endingThree;
        }

        if ($count == 1) {
            return $endingOne;
        }

        return $endingTwo;
    }

    public function getName()
    {
        return 'pluralization_russian';
    }
}