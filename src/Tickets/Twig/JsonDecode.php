<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 03.08.15
 * Time: 20:30
 */

namespace Tickets\Twig;

use \Twig_Extension;

class JsonDecode extends Twig_Extension
{
    public function getName()
    {
        return 'json_decode';
    }

    public function getFilters() {
        return [
            'json_decode' => new \Twig_Filter_Method($this, 'jsonDecode'),
        ];
    }

    public function jsonDecode($string) {
        return json_decode($string, true);
    }
}