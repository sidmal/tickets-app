<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 19.08.15
 * Time: 20:53
 */

namespace Tickets\TicketChooserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MerchantTestInputAccessTokenType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'accessToken',
                'text',
                [
                    'label' => 'Ключ доступа к API',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ]
            )
            ->add(
                'button',
                'submit',
                [
                    'label' => 'Сгенерировать форму',
                    'attr' => [
                        'class' => 'btn btn-primary'
                    ]
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ticket_chooser_merchant_test_access_token_input_form';
    }
}