<?php

namespace Tickets\TicketChooserBundle\Admin;

use AppBundle\Admin\AppSonataAdmin;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TicketTypesAdmin extends AppSonataAdmin
{
    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, ['label' => 'ID'])
            ->add('name', null, ['label' => 'Наименование'])
            ->add('merchant', null, ['label' => 'Мерчант'])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->add(
                'name',
                null,
                [
                    'label' => 'Наименование',
                    'template' => 'TicketsTicketChooserBundle:TicketTypes:list_field_name.html.twig'
                ]
            )
            ->add('description', null, ['label' => 'Описание'])
            ->add('merchant', null, ['label' => 'Мерчант'])
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'edit' => ['template' => 'TicketsTicketChooserBundle:Merchants:list_btn_edit_small.html.twig'],
                        'delete' => ['template' => 'TicketsTicketChooserBundle:TicketTypes:list_btn_delete_small.html.twig']
                    ]
                ]
            )
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'merchant',
                null,
                [
                    'label' => 'Выбирите мерчант',
                    'required' => true,
                    'query_builder' => $this->getEm()->getRepository('TicketsTicketChooserBundle:Merchants')
                        ->getChoiceMerchants($this->getAllowedMerchants()),
                    'empty_data' => null
                ]
            )
            ->add('name', null, ['label' => 'Введите наименование'])
            ->add('description', null, ['label' => 'Введите описание'])
        ;
    }
}
