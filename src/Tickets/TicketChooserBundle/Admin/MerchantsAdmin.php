<?php

namespace Tickets\TicketChooserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class MerchantsAdmin extends Admin
{
    /**
     * @var array
     */
    protected $aggregators;

    /**
     * @param array $aggregators
     * @return $this
     */
    public function setAggregators(array $aggregators)
    {
        $this->aggregators = $aggregators;

        return $this;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, ['label' => 'ID'])
            ->add('name', null, ['label' => 'Наименование мерчанта'])
            ->add('domain', null, ['label' => 'Доменное имя'])
            ->add(
                'aggregatorHandlerImplementation',
                null,
                ['label' => 'Агрегатор'],
                'choice',
                ['choices' => $this->aggregators]
            )
            ->add('paymentAggregatorMerchantId', null, ['label' => 'ID мерчанта агрегатора'])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->add('name', null, ['label' => 'Наименование мерчанта'])
            ->add('domain', null, ['label' => 'Доменное имя'])
            ->add(
                'aggregatorHandlerImplementation',
                null,
                [
                    'label' => 'Агрегатор',
                    'template' => 'TicketsTicketChooserBundle:Merchants:list_field_aggregator_implementation.html.twig',
                    'aggregators' => $this->aggregators
                ]
            )
            ->add('paymentAggregatorMerchantId', null, ['label' => 'ID мерчанта агрегатора'])
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'edit' => ['template' => 'TicketsTicketChooserBundle:Merchants:list_btn_edit_small.html.twig'],
                        'delete' => ['template' => 'TicketsTicketChooserBundle:Merchants:list_btn_delete_small.html.twig']
                    ]
                ]
            )
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', ['label' => 'Наименование мерчанта'])
            ->add('promo', 'textarea', ['label' => 'Описание мерчанта'])
            ->add(
                'periodsDescription',
                'textarea',
                [
                    'label' => 'Описание периода действия билета (текст подсказки)',
                    'required' => false
                ]
            )
            ->add(
                'address',
                'textarea',
                [
                    'label' => 'Адрес расположения (указывается на билете)',
                    'required' => false
                ]
            )
            ->add(
                'howToGet',
                'textarea',
                [
                    'label' => 'Как добраться (указывается на билете)',
                    'required' => false
                ]
            )
            ->add(
                'domain',
                'text',
                [
                    'label' => 'Доменное имя мерчанта',
                    'help' => 'Пожалуйста будьте внимательны, данное поле должно соответствовать заголовку "Original" отправляемому вашим сайтом.'
                ]
            )
            ->add('ageLimit', 'text', ['label' => 'Возрастное ограничение'])
            ->add(
                'typesDisable',
                'checkbox',
                [
                    'label' => 'Нет типов билетов',
                    'required' => false
                ]
            )
            ->add(
                'accessToken', 'text', ['label' => 'Ключ доступа к API', 'required' => false]
            )
            ->add(
                'paymentAggregatorMerchantId',
                'text',
                [
                    'label' => 'Идентификатор мерчанта агрегатора (для совершения платежа)',
                    'required' => false
                ]
            )
            ->add(
                'paymentAggregatorMerchantSecretKey',
                'text',
                [
                    'label' => 'Секретный ключ мерчанта агрегатора (для совершения платежа)',
                    'required' => false
                ]
            );

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $formMapper->add(
                'aggregatorHandlerImplementation',
                'choice',
                [
                    'label' => 'Обработчик запросов платежного агрегатора',
                    'required' => false,
                    'choices' => $this->aggregators
                ]
            );
        }
    }
}
