<?php

namespace Tickets\TicketChooserBundle\Admin;

use AppBundle\Admin\AppSonataAdmin;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TicketsAdmin extends AppSonataAdmin
{
    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, ['label' => 'ID'])
            ->add('merchant', null, ['label' => 'Мерчант'])
            ->add('type', null, ['label' => 'Тип билета'])
            ->add('name', null, ['label' => 'Наименование'])
            ->add('price', null, ['label' => 'Цена, руб.'])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->add('name', null, ['label' => 'Наименование'])
            ->add('price', null, ['label' => 'Цена, руб.'])
            ->add('merchant', null, ['label' => 'Мерчант'])
            ->add('type', null, ['label' => 'Тип билета'])
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => ['template' => 'TicketsTicketChooserBundle:Merchants:list_btn_edit_small.html.twig'],
                    'delete' => ['template' => 'TicketsTicketChooserBundle:TicketTypes:list_btn_delete_small.html.twig']
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'merchant',
                null,
                [
                    'label' => 'Мерчант',
                    'required' => true,
                    'query_builder' => $this->getEm()->getRepository('TicketsTicketChooserBundle:Merchants')
                        ->getChoiceMerchants($this->getAllowedMerchants()),
                    'empty_data' => null
                ]
            )
            ->add(
                'type',
                null,
                [
                    'label' => 'Тип билета',
                    'required' => true,
                    'query_builder' => function (EntityRepository $er) {
                        $qb = $er->createQueryBuilder('ticket_types');

                        $merchants = $this->getAllowedMerchants();
                        if (empty($merchants) || count($merchants) <= 0) {
                            return $qb;
                        }

                        $qb->andWhere($qb->expr()->in('ticket_types.merchant', $merchants));

                        return $qb;
                    }
                ]
            )
            ->add('name', null, ['label' => 'Наименование'])
            ->add('price', null, ['label' => 'Цена, руб.'])
            ->add('tooltip', null, ['label' => 'Подсказка', 'required' => false])
        ;
    }
}
