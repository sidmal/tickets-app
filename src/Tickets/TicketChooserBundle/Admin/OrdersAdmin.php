<?php

namespace Tickets\TicketChooserBundle\Admin;

use AppBundle\Admin\AppSonataAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Tickets\TicketChooserBundle\Entity\Orders;

class OrdersAdmin extends AppSonataAdmin
{
    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
        $collection->remove('create');
        $collection->remove('delete');

        $collection->add('send_ticket', $this->getRouterIdParameter().'/send_ticket');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, ['label' => 'ID билета'])
            ->add('amount', null, ['label' => 'Сумма, руб.'])
            ->add('account', null, ['label' => 'Email пользователя'])
            ->add(
                'paymentMethod',
                null,
                ['label' => 'Способ оплаты'],
                'choice',
                [
                    'choices' => $this->getConfigurationPool()->getContainer()->getParameter('payment_method')
                ]
            )
            ->add(
                'state',
                null,
                ['label' => 'Статус'],
                'choice',
                [
                    'choices' => Orders::getStateDescription()
                ]
            )
            ->add('aggregatorBillId', null, ['label' => 'ID счета ПС'])
            ->add(
                'createdAt',
                'doctrine_orm_datetime',
                [
                    'label' => 'Дата создания',
                    'field_type' => 'sonata_type_datetime_picker',
                    'dp_use_seconds' => false
                ],
                null,
                [
                    'widget' => 'single_text',
                    'format' => 'dd.MM.yyyy',
                    'attr' => ['class' => 'datetimepicker']
                ]
            )
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->add(
                'id',
                null,
                [
                    'label' => 'ID билета',
                    'template' => 'TicketsTicketChooserBundle:Orders:list_field_id.html.twig'
                ]
            )
            ->add('amount', null, ['label' => 'Сумма, руб.'])
            ->add('account', null, ['label' => 'Email пользователя'])
            ->add(
                'chooseTickets',
                null,
                [
                    'label' => 'Оплачиваемые услуги',
                    'template' => 'TicketsTicketChooserBundle:Orders:list_field_choose_ticket.html.twig'
                ]
            )
            ->add('aggregatorBillId', null, ['label' => 'ID счета ПС'])
            ->add(
                'paymentMethod',
                null,
                [
                    'label' => 'Способ оплаты',
                    'template' => 'TicketsTicketChooserBundle:Orders:list_field_payment_system.html.twig'
                ]
            )
            ->add(
                'state',
                null,
                [
                    'label' => 'Статус',
                    'template' => 'TicketsTicketChooserBundle:Orders:list_field_state.html.twig'
                ]
            )
            ->add('createdAt', null, ['label' => 'Дата создания', 'format' => 'd.m.Y H:i:s'])
            ->add(
                '_action',
                'actions', [
                'actions' => [
                    'edit' => ['template' => 'TicketsTicketChooserBundle:Merchants:list_btn_edit_small.html.twig'],
                    'send_ticket' => ['template' => 'TicketsTicketChooserBundle:Orders:list_btn_resend_email.html.twig']
                ]
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('state', 'choice', ['label' => 'Статус', 'choices' => Orders::getStateDescription()])
        ;
    }
}
