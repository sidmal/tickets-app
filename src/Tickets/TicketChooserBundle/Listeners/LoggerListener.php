<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 22.05.15
 * Time: 15:33
 */

namespace Tickets\TicketChooserBundle\Listeners;

use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Tickets\TicketChooserBundle\Interfaces\LoggerInterface;

class LoggerListener
{
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof LoggerInterface) {
            $event->getRequest()->attributes->set('api_log', true);
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$event->getRequest()->attributes->get('api_log')) {
            return;
        }

        $toLog = 'date: '.(new \DateTime('now'))->format('Y-m-d H:i:s')."\n";
        $toLog .= 'request:'."\n";
        $toLog .= "\t".'headers: '."\n";
        $toLog .= "\t\t".print_r($event->getRequest()->headers->all(), true)."\n";
        $toLog .= "\t".'content: '."\n";
        $toLog .= "\t\t".print_r($event->getRequest()->getRequestUri(), true)."\n";
        $toLog .= "\t".'post: '."\n";
        $toLog .= "\t\t".print_r($event->getRequest()->request->all(), true)."\n";
        $toLog .= "\t".'get: '."\n";
        $toLog .= "\t\t".print_r($event->getRequest()->query->all(), true)."\n";
        $toLog .= 'response:'."\n";
        $toLog .= "\t".'headers: '."\n";
        $toLog .= "\t\t".print_r($event->getResponse()->headers->all(), true)."\n";
        $toLog .= "\t".'content: '."\n";
        $toLog .= "\t\t".print_r($event->getResponse()->getContent(), true)."\n";

        $this->logger->addInfo($toLog);
    }
}