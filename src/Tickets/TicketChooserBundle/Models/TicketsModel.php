<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 03.08.15
 * Time: 11:53
 */

namespace Tickets\TicketChooserBundle\Models;

use Doctrine\ORM\EntityManager;

class TicketsModel
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('TicketsTicketChooserBundle:Tickets');
    }

    /**
     * @param $price
     * @param $type
     * @return null|\Tickets\TicketChooserBundle\Entity\Tickets
     */
    public function getTicketNameBy($price, $type)
    {
        if ($ticket = $this->repository->findOneBy(['price' => $price, 'type' => $type])) {
            return $ticket;
        }

        return null;
    }
}