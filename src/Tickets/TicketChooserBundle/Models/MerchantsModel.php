<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 20.05.15
 * Time: 15:09
 */

namespace Tickets\TicketChooserBundle\Models;

use Doctrine\ORM\EntityManager;

class MerchantsModel
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $merchantsRepository;

    const MERCHANT_DATA_SUCCESS = 0;
    const ERROR_ACCESS_DENIED = 1;
    const ERROR_CONFIGURATION_INCORRECT = 2;
    const ERROR_UNKNOWN = 1000;

    private static $errorDescription = [
        self::MERCHANT_DATA_SUCCESS => 'Success.',
        self::ERROR_ACCESS_DENIED => 'Access denied. Your access key is invalid.',
        self::ERROR_CONFIGURATION_INCORRECT => 'Error in process generate library. Merchant configuration is incorrect.',
        self::ERROR_UNKNOWN => 'Unknown error.'
    ];

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->merchantsRepository = $this->em->getRepository('TicketsTicketChooserBundle:Merchants');
    }

    public function getMerchantDataForJsApi($merchantKey)
    {
        if (!$merchant = $this->merchantsRepository->findOneBy(['accessToken' => $merchantKey])) {
            return self::ERROR_ACCESS_DENIED;
        }

        if (!$merchant->getPeriods() || !$merchant->getTicketTypes() || !$merchant->getTickets()) {
            return self::ERROR_CONFIGURATION_INCORRECT;
        }

        $jsApiData = [
            'merchant' => [
                'name' => $merchant->getName(),
                'promo' => $merchant->getPromo(),
                'periodsDescription' => $merchant->getPeriodsDescription(),
                'merchantId' => $merchant->getId(),
                'chosenType' => ($merchant->getTypesDisable()) ? 0 : null,
                'ageLimit' => $merchant->getAgeLimit(),
            ],
            'periods' => [],
            'ticketTypes' => []
        ];

        /**
         * @var \Tickets\TicketChooserBundle\Entity\Periods $period
         */
        foreach ($merchant->getPeriods() as $key => $period) {
            $jsApiData['periods'][] = ['name' => $period->getName()];
        }

        /**
         * @var \Tickets\TicketChooserBundle\Entity\TicketTypes $ticketType
         */
        foreach ($merchant->getTicketTypes() as $ticketType) {
            $ticketTypeRow = [
                'name' => $ticketType->getName(),
                'description' => $ticketType->getDescription(),
                'id' => $ticketType->getId(),
                'tickets' => []];

            /**
             * @var \Tickets\TicketChooserBundle\Entity\Tickets $ticket
             */
            foreach ($ticketType->getTickets() as $ticket) {
                $ticketTypeRow['tickets'][] = [
                    'name' => $ticket->getName(),
                    'price' => $ticket->getPrice(),
                    'tooltip' => $ticket->getTooltip() ? $ticket->getTooltip() : null
                ];
            }

            $jsApiData['ticketTypes'][] = $ticketTypeRow;
        }

        return $jsApiData;
    }

    static public function getStatusDescription($status)
    {
        return !isset(self::$errorDescription[$status]) ? self::$errorDescription[self::ERROR_UNKNOWN] : self::$errorDescription[$status];
    }
}