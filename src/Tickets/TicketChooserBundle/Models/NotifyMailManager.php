<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 04.08.15
 * Time: 10:02
 */

namespace Tickets\TicketChooserBundle\Models;

use Knp\Snappy\GeneratorInterface;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Tickets\TicketChooserBundle\Entity\Orders;

class NotifyMailManager
{
    /**
     * @var TwigEngine
     */
    protected $templating;

    /**
     * @var string
     */
    protected $template;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var string
     */
    protected $from;

    /**
     * @var GeneratorInterface
     */
    protected $pdf;

    /**
     * @var string
     */
    protected $path;

    public function __construct(TwigEngine $templating, $template, \Swift_Mailer $mailer, $from)
    {
        $this->templating = $templating;
        $this->template = $template;
        $this->mailer = $mailer;
        $this->from = $from;
    }

    /**
     * @param GeneratorInterface $pdf
     */
    public function setPdf(GeneratorInterface $pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @param Orders $order
     * @param null|string $subject
     * @throws \Exception
     * @throws \Twig_Error
     */
    public function sendEmail(Orders $order, $subject = null)
    {
        if (!$subject || !is_string($subject)) {
            $subject = sprintf('Билет "%s"', $order->getMerchant()->getName());
        }

        /** @var \Swift_Message $message */
        $message = \Swift_Message::newInstance();

        $messageBody = $this->templating->render(
            '@'.$this->template,
            [
                'merchantName' => $order->getMerchant()->getName(),
                'merchantDomain' => $order->getMerchant()->getDomain(),
                'merchantAddress' => $order->getMerchant()->getAddress(),
                'merchantHowToGet' => $order->getMerchant()->getHowToGet(),
                'orderAmount' => $order->getAmount(),
                'ticketType' => $order->getChooseTicketType()->getName(),
                'orderId' => $order->getId(),
                'validFrom' => $order->getUpdatedAt(),
                'validTo' => $order->getUpdatedAt()->add(new \DateInterval('P1M')),
                'ticketDescription' => $order->getChooseTicketType()->getDescription()
            ]
        );

        if ($this->pdf && $this->pdf instanceof GeneratorInterface) {
            $filePath = sprintf('%s%012d.pdf', $this->path, $order->getId());

            if (!file_exists($filePath)) {
                $this->pdf->generateFromHtml(
                    $messageBody,$filePath, ['orientation' => 'landscape']
                );
            }

            $message->attach(\Swift_Attachment::fromPath($filePath));
        }

        $message
            ->setSubject($subject)
            ->setFrom([$this->from => $order->getMerchant()->getName()])
            ->setTo($order->getAccount())
            ->setBody($messageBody, 'text/html');

        $this->mailer->send($message);
    }
}