<?php

namespace Tickets\TicketChooserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Tickets\TicketChooserBundle\Traits\EntityTimePoints;

/**
 * Tickets
 *
 * @ORM\Table(name="tickets")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Tickets
{
    use EntityTimePoints;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var \Tickets\TicketChooserBundle\Entity\TicketTypes
     *
     * @ORM\ManyToOne(targetEntity="Tickets\TicketChooserBundle\Entity\TicketTypes", inversedBy="tickets")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @var \Tickets\TicketChooserBundle\Entity\Merchants
     *
     * @ORM\ManyToOne(targetEntity="Tickets\TicketChooserBundle\Entity\Merchants", inversedBy="tickets")
     * @ORM\JoinColumn(name="merchant_id", referencedColumnName="id")
     */
    private $merchant;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip", type="text", nullable=true)
     */
    private $tooltip;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tickets
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Tickets
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set type
     *
     * @param \Tickets\TicketChooserBundle\Entity\TicketTypes $type
     * @return Tickets
     */
    public function setType(\Tickets\TicketChooserBundle\Entity\TicketTypes $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Tickets\TicketChooserBundle\Entity\TicketTypes 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set merchant
     *
     * @param \Tickets\TicketChooserBundle\Entity\Merchants $merchant
     * @return Tickets
     */
    public function setMerchant(\Tickets\TicketChooserBundle\Entity\Merchants $merchant = null)
    {
        $this->merchant = $merchant;

        return $this;
    }

    /**
     * Get merchant
     *
     * @return \Tickets\TicketChooserBundle\Entity\Merchants 
     */
    public function getMerchant()
    {
        return $this->merchant;
    }

    /**
     * @param string $tooltip
     * @return $this
     */
    public function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    /**
     * @return string
     */
    public function getTooltip()
    {
        return $this->tooltip;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getName() : 'Новый билет';
    }
}
