<?php

namespace Tickets\TicketChooserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Tickets\TicketChooserBundle\Traits\EntityTimePoints;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TicketTypes
 *
 * @ORM\Table(name="ticket_types")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class TicketTypes
{
    use EntityTimePoints;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var \Tickets\TicketChooserBundle\Entity\Merchants
     *
     * @ORM\ManyToOne(targetEntity="Tickets\TicketChooserBundle\Entity\Merchants", inversedBy="ticketTypes")
     * @ORM\JoinColumn(name="merchant_id", referencedColumnName="id")
     */
    private $merchant;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Tickets\TicketChooserBundle\Entity\Tickets", mappedBy="type")
     */
    private $tickets;

    public function __construct()
    {
        $this->tickets = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TicketTypes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TicketTypes
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set merchant
     *
     * @param \Tickets\TicketChooserBundle\Entity\Merchants $merchant
     * @return TicketTypes
     */
    public function setMerchant(\Tickets\TicketChooserBundle\Entity\Merchants $merchant = null)
    {
        $this->merchant = $merchant;

        return $this;
    }

    /**
     * Get merchant
     *
     * @return \Tickets\TicketChooserBundle\Entity\Merchants 
     */
    public function getMerchant()
    {
        return $this->merchant;
    }

    /**
     * Add tickets
     *
     * @param \Tickets\TicketChooserBundle\Entity\Tickets $tickets
     * @return TicketTypes
     */
    public function addTicket(\Tickets\TicketChooserBundle\Entity\Tickets $tickets)
    {
        $this->tickets[] = $tickets;

        return $this;
    }

    /**
     * Remove tickets
     *
     * @param \Tickets\TicketChooserBundle\Entity\Tickets $tickets
     */
    public function removeTicket(\Tickets\TicketChooserBundle\Entity\Tickets $tickets)
    {
        $this->tickets->removeElement($tickets);
    }

    /**
     * Get tickets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId()
            ? str_replace(['<br />', '<br>', '<br/>'], ' ', $this->getName()) : 'Новый тип билета';
    }
}
