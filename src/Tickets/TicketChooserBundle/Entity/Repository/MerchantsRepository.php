<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 27.08.15
 * Time: 20:35
 */

namespace Tickets\TicketChooserBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class MerchantsRepository extends EntityRepository
{
    public function getChoiceMerchants(array $merchants)
    {
        $qb = $this->createQueryBuilder('merchants');

        if (empty($merchants) || count($merchants) <= 0) {
            return $qb;
        }

        $qb->andWhere($qb->expr()->in('merchants.id', $merchants));

        return $qb;
    }
}