<?php

namespace Tickets\TicketChooserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Tickets\TicketChooserBundle\Traits\EntityTimePoints;

/**
 * Orders
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity
 *
 * @ORM\HasLifecycleCallbacks
 */
class Orders
{
    use EntityTimePoints;

    /**
     * счет выставлен
     */
    const PAYMENT_INIT = 0;

    /**
     * счет оплачен
     */
    const PAYMENT_COMPLETE = 1;

    /**
     * билет использован
     */
    const PAYMENT_CLOSED = 2;

    static protected $stateDescription = [
        self::PAYMENT_INIT => 'Счет выставлен',
        self::PAYMENT_COMPLETE => 'Счет оплачен',
        self::PAYMENT_CLOSED => 'Билет использован'
    ];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="account", type="string", length=255)
     */
    private $account;

    /**
     * @var string
     *
     * @ORM\Column(name="aggregator_merchant_id", type="string", length=255)
     */
    private $aggregatorMerchantId;

    /**
     * @var string
     *
     * @ORM\Column(name="choose_tickets", type="text")
     */
    private $chooseTickets;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_method", type="string", length=255)
     */
    private $paymentMethod;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;

    /**
     * @var \Tickets\TicketChooserBundle\Entity\TicketTypes
     *
     * @ORM\ManyToOne(targetEntity="Tickets\TicketChooserBundle\Entity\TicketTypes")
     * @ORM\JoinColumn(name="choose_ticket_type_id", referencedColumnName="id")
     */
    private $chooseTicketType;

    /**
     * @var \Tickets\TicketChooserBundle\Entity\Merchants
     *
     * @ORM\ManyToOne(targetEntity="Tickets\TicketChooserBundle\Entity\Merchants")
     * @ORM\JoinColumn(name="merchant_id", referencedColumnName="id")
     */
    private $merchant;

    /**
     * @var string
     *
     * @ORM\Column(name="aggregator_bill_id", type="string", length=255, nullable=true)
     */
    private $aggregatorBillId;

    public function __construct()
    {
        $this->state = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Orders
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set account
     *
     * @param string $account
     * @return Orders
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return string 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set aggregatorMerchantId
     *
     * @param string $aggregatorMerchantId
     * @return Orders
     */
    public function setAggregatorMerchantId($aggregatorMerchantId)
    {
        $this->aggregatorMerchantId = $aggregatorMerchantId;

        return $this;
    }

    /**
     * Get aggregatorMerchantId
     *
     * @return string 
     */
    public function getAggregatorMerchantId()
    {
        return $this->aggregatorMerchantId;
    }

    /**
     * Set chooseTickets
     *
     * @param string $chooseTickets
     * @return Orders
     */
    public function setChooseTickets($chooseTickets)
    {
        $this->chooseTickets = $chooseTickets;

        return $this;
    }

    /**
     * Get chooseTickets
     *
     * @return string 
     */
    public function getChooseTickets()
    {
        return $this->chooseTickets;
    }

    /**
     * Set paymentMethod
     *
     * @param string $paymentMethod
     * @return Orders
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return string 
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set chooseTicketType
     *
     * @param \Tickets\TicketChooserBundle\Entity\TicketTypes $chooseTicketType
     * @return Orders
     */
    public function setChooseTicketType(\Tickets\TicketChooserBundle\Entity\TicketTypes $chooseTicketType = null)
    {
        $this->chooseTicketType = $chooseTicketType;

        return $this;
    }

    /**
     * Get chooseTicketType
     *
     * @return \Tickets\TicketChooserBundle\Entity\TicketTypes 
     */
    public function getChooseTicketType()
    {
        return $this->chooseTicketType;
    }

    /**
     * Set merchant
     *
     * @param \Tickets\TicketChooserBundle\Entity\Merchants $merchant
     * @return Orders
     */
    public function setMerchant(\Tickets\TicketChooserBundle\Entity\Merchants $merchant = null)
    {
        $this->merchant = $merchant;

        return $this;
    }

    /**
     * Get merchant
     *
     * @return \Tickets\TicketChooserBundle\Entity\Merchants 
     */
    public function getMerchant()
    {
        return $this->merchant;
    }

    /**
     * @param integer $state
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $aggregatorBillId
     * @return $this
     */
    public function setAggregatorBillId($aggregatorBillId)
    {
        $this->aggregatorBillId = $aggregatorBillId;

        return $this;
    }

    /**
     * @return string
     */
    public function getAggregatorBillId()
    {
        return $this->aggregatorBillId;
    }

    /**
     * @return array
     */
    public static function getStateDescription()
    {
        return self::$stateDescription;
    }

    public function __toString()
    {
        return $this->getId() ? sprintf('Заказ №%012d', $this->getId()) : 'Новый заказ';
    }
}
