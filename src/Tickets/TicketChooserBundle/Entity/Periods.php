<?php

namespace Tickets\TicketChooserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Tickets\TicketChooserBundle\Traits\EntityTimePoints;

/**
 * Periods
 *
 * @ORM\Table(name="periods")
 * @ORM\Entity
 */
class Periods
{
    use EntityTimePoints;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \Tickets\TicketChooserBundle\Entity\Merchants
     *
     * @ORM\ManyToOne(targetEntity="Tickets\TicketChooserBundle\Entity\Merchants", inversedBy="periods")
     * @ORM\JoinColumn(name="merchant_id", referencedColumnName="id")
     */
    private $merchant;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Periods
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set merchant
     *
     * @param \Tickets\TicketChooserBundle\Entity\Merchants $merchant
     * @return Periods
     */
    public function setMerchant(\Tickets\TicketChooserBundle\Entity\Merchants $merchant = null)
    {
        $this->merchant = $merchant;

        return $this;
    }

    /**
     * Get merchant
     *
     * @return \Tickets\TicketChooserBundle\Entity\Merchants 
     */
    public function getMerchant()
    {
        return $this->merchant;
    }
}
