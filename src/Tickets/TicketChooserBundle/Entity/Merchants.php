<?php

namespace Tickets\TicketChooserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Tickets\TicketChooserBundle\Traits\EntityTimePoints;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Merchants
 *
 * @ORM\Table(name="merchants")
 * @ORM\Entity(repositoryClass="Tickets\TicketChooserBundle\Entity\Repository\MerchantsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Merchants
{
    use EntityTimePoints;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="promo", type="text")
     */
    private $promo;

    /**
     * @var string
     *
     * @ORM\Column(name="periods_description", type="text", nullable=true)
     */
    private $periodsDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="access_token", type="string", length=255, unique=true)
     */
    private $accessToken;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_aggregator_merchant_id", type="string", length=255, nullable=true)
     */
    private $paymentAggregatorMerchantId;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_aggregator_merchant_secret_key", type="string", length=255, nullable=true)
     */
    private $paymentAggregatorMerchantSecretKey;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Tickets\TicketChooserBundle\Entity\TicketTypes", mappedBy="merchant", cascade={"persist"}, orphanRemoval=true)
     */
    private $ticketTypes;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Tickets\TicketChooserBundle\Entity\Tickets", mappedBy="merchant")
     */
    private $tickets;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Tickets\TicketChooserBundle\Entity\Periods", mappedBy="merchant")
     */
    private $periods;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=255)
     */
    private $domain;

    /**
     * @var string
     *
     * @ORM\Column(name="aggregator_handler_implementation", type="string", length=255, nullable=true)
     */
    private $aggregatorHandlerImplementation;

    /**
     * @var string
     *
     * @ORM\Column(name="age_limit", type="string", length=10)
     */
    private $ageLimit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="types_disabled", type="boolean", options={"default":false})
     */
    private $typesDisable;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="merchants")
     * @ORM\JoinColumn(name="related_user_id", referencedColumnName="id", nullable=true)
     */
    private $relatedUser;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="how_to_get", type="text", nullable=true)
     */
    private $howToGet;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ticketTypes = new ArrayCollection();
        $this->tickets = new ArrayCollection();
        $this->periods = new ArrayCollection();

        $this->aggregatorHandlerImplementation = null;
        $this->relatedUser = null;

        $this->typesDisable = false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Merchants
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set promo
     *
     * @param string $promo
     * @return Merchants
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * Get promo
     *
     * @return string 
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * Set periodsDescription
     *
     * @param string $periodsDescription
     * @return Merchants
     */
    public function setPeriodsDescription($periodsDescription)
    {
        $this->periodsDescription = $periodsDescription;

        return $this;
    }

    /**
     * Get periodsDescription
     *
     * @return string 
     */
    public function getPeriodsDescription()
    {
        return $this->periodsDescription;
    }

    /**
     * Set accessToken
     *
     * @param string $accessToken
     * @return Merchants
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * Get accessToken
     *
     * @return string 
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Set paymentAggregatorMerchantId
     *
     * @param string $paymentAggregatorMerchantId
     * @return Merchants
     */
    public function setPaymentAggregatorMerchantId($paymentAggregatorMerchantId)
    {
        $this->paymentAggregatorMerchantId = $paymentAggregatorMerchantId;

        return $this;
    }

    /**
     * Get paymentAggregatorMerchantId
     *
     * @return string 
     */
    public function getPaymentAggregatorMerchantId()
    {
        return $this->paymentAggregatorMerchantId;
    }

    /**
     * Set paymentAggregatorMerchantSecretKey
     *
     * @param string $paymentAggregatorMerchantSecretKey
     * @return Merchants
     */
    public function setPaymentAggregatorMerchantSecretKey($paymentAggregatorMerchantSecretKey)
    {
        $this->paymentAggregatorMerchantSecretKey = $paymentAggregatorMerchantSecretKey;

        return $this;
    }

    /**
     * Get paymentAggregatorMerchantSecretKey
     *
     * @return string 
     */
    public function getPaymentAggregatorMerchantSecretKey()
    {
        return $this->paymentAggregatorMerchantSecretKey;
    }

    /**
     * Add ticketTypes
     *
     * @param \Tickets\TicketChooserBundle\Entity\TicketTypes $ticketTypes
     * @return Merchants
     */
    public function addTicketType(\Tickets\TicketChooserBundle\Entity\TicketTypes $ticketTypes)
    {
        $this->ticketTypes[] = $ticketTypes;

        return $this;
    }

    /**
     * Remove ticketTypes
     *
     * @param \Tickets\TicketChooserBundle\Entity\TicketTypes $ticketTypes
     */
    public function removeTicketType(\Tickets\TicketChooserBundle\Entity\TicketTypes $ticketTypes)
    {
        $this->ticketTypes->removeElement($ticketTypes);
    }

    /**
     * Get ticketTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTicketTypes()
    {
        return $this->ticketTypes;
    }

    /**
     * Add tickets
     *
     * @param \Tickets\TicketChooserBundle\Entity\Tickets $tickets
     * @return Merchants
     */
    public function addTicket(\Tickets\TicketChooserBundle\Entity\Tickets $tickets)
    {
        $this->tickets[] = $tickets;

        return $this;
    }

    /**
     * Remove tickets
     *
     * @param \Tickets\TicketChooserBundle\Entity\Tickets $tickets
     */
    public function removeTicket(\Tickets\TicketChooserBundle\Entity\Tickets $tickets)
    {
        $this->tickets->removeElement($tickets);
    }

    /**
     * Get tickets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * Add periods
     *
     * @param \Tickets\TicketChooserBundle\Entity\Periods $periods
     * @return Merchants
     */
    public function addPeriod(\Tickets\TicketChooserBundle\Entity\Periods $periods)
    {
        $this->periods[] = $periods;

        return $this;
    }

    /**
     * Remove periods
     *
     * @param \Tickets\TicketChooserBundle\Entity\Periods $periods
     */
    public function removePeriod(\Tickets\TicketChooserBundle\Entity\Periods $periods)
    {
        $this->periods->removeElement($periods);
    }

    /**
     * Get periods
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPeriods()
    {
        return $this->periods;
    }

    /**
     * @param string $domain
     * @return $this
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param string $aggregatorHandlerImplementation
     * @return $this
     */
    public function setAggregatorHandlerImplementation($aggregatorHandlerImplementation)
    {
        $this->aggregatorHandlerImplementation = $aggregatorHandlerImplementation;

        return $this;
    }

    /**
     * @return string
     */
    public function getAggregatorHandlerImplementation()
    {
        return $this->aggregatorHandlerImplementation;
    }

    /**
     * @param string $ageLimit
     * @return $this
     */
    public function setAgeLimit($ageLimit)
    {
        $this->ageLimit = $ageLimit;

        return $this;
    }

    /**
     * @return string
     */
    public function getAgeLimit()
    {
        return $this->ageLimit;
    }

    public function setTypesDisable($typesDisable)
    {
        $this->typesDisable = $typesDisable;

        return $this;
    }

    public function getTypesDisable()
    {
        return $this->typesDisable;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getId() ? $this->getName() : 'Новый мерчант';
    }

    /**
     * @ORM\PrePersist()
     */
    public function addAccessToken()
    {
        if (!$this->getAccessToken()) {
            $this->setAccessToken(md5(uniqid(rand(), true)));
        }
    }

    /**
     * Set relatedUser
     *
     * @param \Application\Sonata\UserBundle\Entity\User $relatedUser
     * @return Merchants
     */
    public function setRelatedUser(\Application\Sonata\UserBundle\Entity\User $relatedUser = null)
    {
        $this->relatedUser = $relatedUser;

        return $this;
    }

    /**
     * Get relatedUser
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getRelatedUser()
    {
        return $this->relatedUser;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Merchants
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set howToGet
     *
     * @param string $howToGet
     * @return Merchants
     */
    public function setHowToGet($howToGet)
    {
        $this->howToGet = $howToGet;

        return $this;
    }

    /**
     * Get howToGet
     *
     * @return string 
     */
    public function getHowToGet()
    {
        return $this->howToGet;
    }
}
