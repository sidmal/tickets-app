<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 19.08.15
 * Time: 12:16
 */

namespace Tickets\TicketChooserBundle\Security;

use Application\Sonata\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\AbstractVoter;
use Symfony\Component\Security\Core\User\UserInterface;
use Tickets\TicketChooserBundle\Entity\Orders;
use Tickets\TicketChooserBundle\Entity\Tickets;
use Tickets\TicketChooserBundle\Entity\TicketTypes;

class MerchantVoter extends AbstractVoter
{
    protected static $supportedAttributes = ['list', 'view', 'edit'];

    protected function getSupportedAttributes()
    {
        return self::$supportedAttributes;
    }

    protected function getSupportedClasses()
    {
        return [
            'Tickets\TicketChooserBundle\Entity\TicketTypes',
            'Tickets\TicketChooserBundle\Entity\Tickets',
            'Tickets\TicketChooserBundle\Entity\Orders'
        ];
    }

    protected function isGranted($attribute, $object, $user = null)
    {
        if (!$user instanceof UserInterface) {
            return false;
        }

        if (!$user instanceof User) {
            throw new \LogicException('The user is somehow not our User class!');
        }

        $merchant = $object->getMerchant();
        if (!$merchant) {
            return false;
        }

        switch($attribute) {
            case self::$supportedAttributes[0]:
                break;
            case self::$supportedAttributes[1]:
                if (!$post->isPrivate()) {
                    return true;
                }

                break;
            case self::$supportedAttributes[2]:
                if ($user->getId() === $post->getOwner()->getId()) {
                    return true;
                }

                break;
        }

        return false;
    }
}