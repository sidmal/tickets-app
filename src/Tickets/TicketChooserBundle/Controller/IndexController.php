<?php

namespace Tickets\TicketChooserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tickets\TicketChooserBundle\Form\MerchantTestInputAccessTokenType;

class IndexController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/", name="index_action")
     * @Template("@TicketsTicketChooser/Default/index.html.twig")
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/example", name="example_action")
     * @Template("@TicketsTicketChooser/Default/example.html.twig")
     */
    public function exampleAction()
    {
        return [];
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/merchant-test", name="merchant_test_action")
     * @Template("@TicketsTicketChooser/Default/merchant_test.html.twig")
     */
    public function merchantTestAction(Request $request)
    {
        $form = $this->createForm(new MerchantTestInputAccessTokenType());

        $merchant = null;

        if ($request->getMethod() == 'POST') {
            $form->submit($request);


            if (!empty($form->getData()['accessToken'])) {
                $merchant = $this->container->get('tickets_chooser.merchant.model')
                    ->getMerchantDataForJsApi($form->getData()['accessToken']);

                if (!is_array($merchant)) {
                    $merchant = null;
                }
            }
        }

        return [
            'form' => $form->createView(),
            'merchantApiKey' => $merchant === null ? null : $form->getData()['accessToken'],
            'merchantNotFound' => $form->isSubmitted() && $merchant == null ? true : false
        ];
    }
}