<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 30.05.15
 * Time: 0:41
 */

namespace Tickets\TicketChooserBundle\Controller;

use AppBundle\Controller\AppCRUDController;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Tickets\TicketChooserBundle\Entity\Merchants;

class MerchantController extends AppCRUDController
{
    public function editAction($id = null, Request $request = null)
    {
        $templateKey = 'edit';

        $id = $request->get($this->admin->getIdParameter());

        /**
         * @var \Tickets\TicketChooserBundle\Entity\Merchants $object
         */
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $preResponse = $this->preEdit($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);
        $form->handleRequest($request);

        $ticketsForm = $this->createFormBuilder();
        foreach ($object->getTickets() as $ticket) {
            $ticketsForm
                ->add('merchants[tickets][name][]', 'text')
                ->add('merchants[tickets][price][]', 'number')
                ->add('merchants[tickets][tooltip][]', 'textarea')
            ;
        }
        $ticketsForm->getForm();

        if ($form->isSubmitted()) {
            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode($request) || $this->isPreviewApproved($request))) {
                try {
                    $object = $this->admin->update($object);

                    $this->addFlash(
                        'sonata_flash_success',
                        $this->admin->trans(
                            'flash_edit_success',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );

                    // redirect to edit mode
                    return $this->redirectTo($object, $request);
                } catch (ModelManagerException $e) {
                    $this->handleModelManagerException($e);
                }
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form'   => $view,
            'form1' => $ticketsForm,
            'object' => $object,
        ), null, $request);
    }

    /**
     * @param Request $request
     * @param Merchants $object
     * @return Response|null
     */
    protected function preCreate(Request $request, $object)
    {
        if (!$object->getAccessToken()) {

        }
    }
}