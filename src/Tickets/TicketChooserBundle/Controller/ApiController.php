<?php

namespace Tickets\TicketChooserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tickets\TicketChooserBundle\Entity\Orders;
use Tickets\TicketChooserBundle\Interfaces\LoggerInterface;
use Tickets\TicketChooserBundle\Models\MerchantsModel;

/**
 * Class ApiController
 * @package Tickets\TicketChooserBundle\Controller
 *
 * @Route("/api/v1")
 */
class ApiController extends Controller implements LoggerInterface
{
    /**
     * @param string $key
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/ticket-chooser/js/{key}", requirements={"key" = "\S+"}, name="ticket_chooser_js_api")
     * @Method({"GET"})
     */
    public function getJsLibraryAction($key)
    {
        $response = new Response();

        $merchantsModel = $this->container->get('tickets_chooser.merchant.model');

        $merchantJsApiData = $merchantsModel->getMerchantDataForJsApi($key);
        if (!is_array($merchantJsApiData)) {
            $response->setContent($merchantsModel->getStatusDescription($merchantJsApiData));

            if ($merchantJsApiData == MerchantsModel::ERROR_ACCESS_DENIED) {
                $response->setStatusCode(Response::HTTP_FORBIDDEN);
            } else {
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return $response;
        }

        $response->headers->set('Content-Type', 'application/javascript');
        return $this->render('@TicketsTicketChooser/Api/ticket_chooser.js.twig', ['merchantJsApiData' => $merchantJsApiData], $response);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/ticket-chooser/create_order", name="ticket_chooser_api_create_order")
     * @Method({"POST"})
     */
    public function createOrderAction(Request $request)
    {
        $response = new Response();

        $em = $this->getDoctrine();

        $requestParams = json_decode($request->request->get('order_params'), true);

        if (empty($requestParams)) {
            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent('Parameters for create order not found.');
        } elseif (empty($requestParams['totalAmt']) || empty($requestParams['clientEmail'])
            || empty($requestParams['chooseTickets']) || empty($requestParams['merchantId'])) {
            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent('Parameters for create order is incorrect.');
        }

        if (!$merchant = $em->getRepository('TicketsTicketChooserBundle:Merchants')->find($requestParams['merchantId'])) {
            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent('Merchant for create order not found.');
        }

        if (!$merchant->getPaymentAggregatorMerchantId()) {
            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent('Merchant for create order not found.');
        }

        $merchantJsApiData = $this->container->get('tickets_chooser.merchant.model')
            ->getMerchantDataForJsApi($merchant->getAccessToken());
        if (!is_array($merchantJsApiData) || !$merchantJsApiData['ticketTypes'][$requestParams['chooseTicketType']]['id']) {
            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent('Unknown error.');
        }

        if (!$ticketType = $em->getRepository('TicketsTicketChooserBundle:TicketTypes')
            ->findOneBy(['id' => $merchantJsApiData['ticketTypes'][$requestParams['chooseTicketType']]['id'], 'merchant' => $merchant])) {
            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent('Sending ticket type for sending merchant not found.');
        }

        $controlAmt = 0;
        foreach ($requestParams['chooseTickets'] as $chooseTicket) {
            if ($chooseTicket['count'] <= 0) {
                continue;
            }

            if (!$em->getRepository('TicketsTicketChooserBundle:Tickets')->findOneBy(['merchant' => $merchant, 'type' =>  $ticketType, 'price' => ($chooseTicket['amt']/$chooseTicket['count'])])) {
                return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                    ->setContent('Tickets by merchant, type and price not found.');
            }

            $controlAmt += $chooseTicket['amt'];
        }

        if (!empty($requestParams['additionalAmounts'])) {
            foreach ($requestParams['additionalAmounts'] as $item) {
                $controlAmt += $item['price'];
            }
        }

        if ((float)$requestParams['totalAmt'] != (float)$controlAmt) {
            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent('Order amount is incorrect.');
        }

        $chooseTickets = $requestParams['chooseTickets'];
        if (!empty($requestParams['additionalAmounts'])) {
            $chooseTickets = array_merge($chooseTickets, ['additionalAmounts' => $requestParams['additionalAmounts']]);
        }

        $order = new Orders();
        $order
            ->setAmount($requestParams['totalAmt'])
            ->setAccount($requestParams['clientEmail'])
            ->setAggregatorMerchantId($merchant->getPaymentAggregatorMerchantId())
            ->setChooseTickets(json_encode($chooseTickets))
            ->setPaymentMethod($requestParams['choosePaymentMethod'])
            ->setChooseTicketType($ticketType)
            ->setMerchant($merchant);

        $em->getManager()->persist($order);
        $em->getManager()->flush();

        if (!$requestParams['choosePaymentMethod']) {
            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent('Payment method to aggregator not found.');
        }

        $aggregatorImplementation = $this->container->get('ticket_chooser.payment_aggregator.dengionline.service');

        $createBillParams = new ParameterBag(
            [
                'request' => [
                    'project' => $merchant->getPaymentAggregatorMerchantId(),
                    'amount' => number_format($requestParams['totalAmt'], 2, '.', ''),
                    'paymentCurrency' => 'RUB',
                    'nickname' => $requestParams['clientEmail'],
                    'order_id' => $order->getId(),
                    'comment' => $merchant->getName(),
                    'mode_type' => $requestParams['choosePaymentMethod'],
                    'tickets_merchant' => $merchant->getId()
                ],
                'aggregatorSecretKey' => $merchant->getPaymentAggregatorMerchantSecretKey()
            ]
        );

        return new Response(
            $aggregatorImplementation->createBill($createBillParams),
            Response::HTTP_OK,
            [
                'Access-Control-Allow-Origin' => $this->container->get('kernel')->getEnvironment() == 'dev'
                    ? '*' : $merchant->getDomain()
            ]
        );
    }
}