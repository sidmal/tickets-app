<?php

namespace Tickets\TicketChooserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tickets\TicketChooserBundle\Exceptions\AggregatorResponseException;
use Tickets\TicketChooserBundle\Interfaces\LoggerInterface;
use Tickets\TicketChooserBundle\Services\PaymentAggregatorInterface;

class NotifyController extends Controller implements LoggerInterface
{
    /**
     * @param Request $request
     * @param integer $merchantId
     * @return Response
     *
     * @Route("/payment/notify/{merchantId}", requirements={"merchantId" = "\d+"}, name="ticket_chooser_payment_notify_action")
     * @Method({"GET", "POST"})
     */
    public function notifyAction(Request $request, $merchantId)
    {
        $response = new Response();

        if (!$merchantId) {
            return $response->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setContent('Required parameter with name "tickets_merchant" not found in request.');
        }

        $em = $this->getDoctrine();

        if (!$merchant = $em->getRepository('TicketsTicketChooserBundle:Merchants')->find($merchantId)) {
            return $response->setStatusCode(Response::HTTP_NOT_FOUND)
                ->setContent('Merchant with id '. $request->request->get('tickets_merchant') .' not found.');
        } elseif (!$merchant->getAggregatorHandlerImplementation()) {
            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent('Merchant notify implementation not configure.');
        } else if (!$aggregatorImplementation = $this->container->get($merchant->getAggregatorHandlerImplementation())) {
            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent('Notify implementation service for merchant not found.');
        }

        try {
            /** @var \Tickets\TicketChooserBundle\Services\Response $aggregatorResponse */
            $aggregatorResponse = $aggregatorImplementation->notifyPayAction($request, $merchant);

            if ($aggregatorResponse->isSuccessResponse()) {
                $subject = sprintf('Билет "%s"', $merchant->getName());
                if ($aggregatorResponse->getStatus() == PaymentAggregatorInterface::RESULT_STATUS_DUPLICATE) {
                    $subject .= ' (Повторное письмо)';
                }

                $this->container->get('ticket_chooser.model.notify_mail')->sendEmail(
                    $aggregatorResponse->getOrder(), $subject
                );
            }

            /**
             * @var \Tickets\TicketChooserBundle\Services\PaymentAggregatorInterface $aggregatorImplementation
             */
            $response->headers->set('Content-Type', $aggregatorImplementation->getResponseContentType());
            return $response->setStatusCode(Response::HTTP_OK)->setContent($aggregatorResponse->getResponse());
        } catch (AggregatorResponseException $e) {
            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent('Unknown server error. Please contact with service administrator.'.$e->getMessage());
        }
    }
}