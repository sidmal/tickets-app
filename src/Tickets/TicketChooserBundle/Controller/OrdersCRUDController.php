<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 04.08.15
 * Time: 9:54
 */

namespace Tickets\TicketChooserBundle\Controller;

use AppBundle\Controller\AppCRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tickets\TicketChooserBundle\Entity\Orders;

class OrdersCRUDController extends AppCRUDController
{
    public function sendTicketAction()
    {
        /** @var Orders $object */
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException('unable to find the object');
        }

        if ($object->getState() == Orders::PAYMENT_COMPLETE) {
            $this->container->get('ticket_chooser.model.notify_mail')->sendEmail($object);

            $this->addFlash('sonata_flash_success', 'Сообщение успешно отправлено на почтовый ящик клиента.');
        } else {
            $this->addFlash('sonata_flash_error', 'Сообщение не может быть отправлено при данном статусе оплаты.');
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}