<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 01.08.15
 * Time: 0:22
 */

namespace Tickets\TicketChooserBundle\Services;

use Tickets\TicketChooserBundle\Entity\Orders;
use Tickets\TicketChooserBundle\Exceptions\AggregatorResponseException;

/**
 * Class Response
 * @package Tickets\TicketChooserBundle\Services
 *
 * ответ платежного агрегатора
 */
class Response
{
    /**
     * @var string
     */
    private $response;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var Orders
     */
    private $order;

    /**
     * @param string $response
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * @return string
     * @throws AggregatorResponseException
     */
    public function getResponse()
    {
        if (!$this->response) {
            throw new AggregatorResponseException('response not set.');
        }

        return $this->response;
    }

    /**
     * @param integer $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param Orders $order
     * @return $this
     */
    public function setOrder(Orders $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return Orders
     * @throws AggregatorResponseException
     */
    public function getOrder()
    {
        if (!$this->order) {
            throw new AggregatorResponseException('order not set.');
        }

        return $this->order;
    }

    /**
     * @return bool
     * @throws AggregatorResponseException
     */
    public function isSuccessResponse()
    {
        if (!in_array($this->status,
            [
                PaymentAggregatorInterface::RESULT_STATUS_SUCCESS,
                PaymentAggregatorInterface::RESULT_STATUS_DUPLICATE,
                PaymentAggregatorInterface::RESULT_STATUS_FAIL
            ])) {
            throw new AggregatorResponseException('status not set.');
        }

        return $this->status == PaymentAggregatorInterface::RESULT_STATUS_SUCCESS
            || $this->status == PaymentAggregatorInterface::RESULT_STATUS_DUPLICATE;
    }
}