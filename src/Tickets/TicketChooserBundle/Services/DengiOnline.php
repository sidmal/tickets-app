<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 20.05.15
 * Time: 23:46
 */

namespace Tickets\TicketChooserBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Tickets\TicketChooserBundle\Entity\Merchants;
use Tickets\TicketChooserBundle\Entity\Orders;

class DengiOnline implements PaymentAggregatorInterface
{
    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * @var string
     */
    private $createBillAction;

    /**
     * @var string
     */
    private $createBillMethod;

    /**
     * @var EntityManager
     */
    private $em;

    const CONTENT_TYPE = 'application/xml';

    public function __construct(TwigEngine $templating, EntityManager $em, $createBillAction, $createBillMethod)
    {
        $this->templating = $templating;
        $this->em = $em;

        $this->createBillAction = $createBillAction;
        $this->createBillMethod = $createBillMethod;
    }

    /**
     * {@inheritdoc}
     */
    public function createBill(ParameterBag $parameters)
    {
        return $this->templating->render(
            '@TicketsTicketChooser/PaymentAggregator/dengionline_create_bill.html.twig',
            [
                'parameters' => $parameters->get('request'),
                'signature' => self::getSignature($parameters->get('request'), $parameters->get('aggregatorSecretKey')),
                'createBillAction' => $this->createBillAction,
                'createBillMethod' => $this->createBillMethod
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function notifyCheckAction(Request $request)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function notifyPayAction(Request $request, Merchants $merchant)
    {
        $request = $request->request;
        $response = new Response();

        $response->setStatus(PaymentAggregatorInterface::RESULT_STATUS_FAIL);

        $key = md5($request->get('amount').$request->get('userid').$request->get('paymentid').$merchant->getPaymentAggregatorMerchantSecretKey());
        if (!$request->has('key') || $key != $request->get('key')) {
            return $response->setResponse($this->getNotifyResponse(false, 'Request parameter with name "key" not found or incorrect.'));
        } elseif (!$request->has('signature')) {
            return $response->setResponse($this->getNotifyResponse(false, 'Request parameter with name "signature" not found.'));
        } elseif (!$order = $this->em->getRepository('TicketsTicketChooserBundle:Orders')->find($request->get('orderid'))) {
            return $response->setResponse($this->getNotifyResponse(false, 'Order with id '.$request->get('orderid').' not found.'));
        } elseif ($order->getAmount() != $request->get('amount') || $order->getAccount() != $request->get('userid')) {
            return $response->setResponse($this->getNotifyResponse(false, 'Request parameter "amount" or "userid" value not equal merchant data.'));
        }

        $controlSignature = self::getSignature(
            [
                'project' => $merchant->getPaymentAggregatorMerchantId(),
                'amount' => number_format($request->get('amount'), 2, '.', ''),
                'paymentCurrency' => 'RUB',
                'nickname' => $request->get('userid'),
                'order_id' => $order->getId(),
                'mode_type' => $request->get('paymode'),
                'tickets_merchant' => $merchant->getId()
            ],
            $merchant->getPaymentAggregatorMerchantSecretKey()
        );

        if ($controlSignature != $request->get('signature')) {
            return $response->setResponse($this->getNotifyResponse(false, 'Additional control signature is incorrect.'));
        } elseif ($order->getState() == Orders::PAYMENT_COMPLETE) {
            return $response->setResponse($this->getNotifyResponse(true, 'Order will be successfully complete earlie.', $order->getId()))
                ->setStatus(PaymentAggregatorInterface::RESULT_STATUS_DUPLICATE)->setOrder($order);
        }

        $order
            ->setAggregatorBillId($request->get('paymentid'))
            ->setState(Orders::PAYMENT_COMPLETE);

        $this->em->persist($order);
        $this->em->flush($order);

        return $response->setResponse($this->getNotifyResponse(true, 'Request successfully complete.', $order->getId()))
            ->setStatus(PaymentAggregatorInterface::RESULT_STATUS_SUCCESS)->setOrder($order);
    }

    static protected function getSignature(array $params, $secretKey)
    {
        ksort($params);
        $signatureObject = [];

        foreach ($params as $parameterKey => $parameterValue) {
            if ($parameterKey == 'comment') {
                continue;
            }

            $signatureObject[] = $parameterKey.'='.$parameterValue;
        }

        return hash('sha256', $secretKey.implode('|', $signatureObject).$secretKey);
    }

    private function getNotifyResponse($status, $comment, $orderId = null)
    {
        return $this->templating->render(
            '@TicketsTicketChooser/PaymentAggregator/dengionline_notify_response.xml.twig',
            [ 'orderId' => $orderId, 'status' => $status, 'comment' => $comment ]
        );
    }

    /**+
     * {@inheritdoc}
     */
    public function getResponseContentType()
    {
        return self::CONTENT_TYPE;
    }
}