<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 20.05.15
 * Time: 23:38
 */

namespace Tickets\TicketChooserBundle\Services;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Tickets\TicketChooserBundle\Entity\Merchants;

interface PaymentAggregatorInterface
{
    const RESULT_STATUS_SUCCESS = 0;
    const RESULT_STATUS_DUPLICATE = 1;
    const RESULT_STATUS_FAIL = 999;

    /**
     * Generate query or form for create bill in aggregator
     *
     * @param ParameterBag $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createBill(ParameterBag $parameters);

    /**
     * Notify check request from aggregator
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function notifyCheckAction(Request $request);

    /**
     * Notify pay request from aggregator
     *
     * @param Request $request
     * @param Merchants $merchant
     * @return Response
     */
    public function notifyPayAction(Request $request, Merchants $merchant);

    /**
     * @return string
     */
    public function getResponseContentType();
}