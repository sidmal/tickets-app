<?php
/**
 * Created by PhpStorm.
 * User: dmitriysinichkin
 * Date: 01.08.15
 * Time: 0:25
 */

namespace Tickets\TicketChooserBundle\Exceptions;

class AggregatorResponseException extends \Exception {}